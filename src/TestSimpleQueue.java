
public class TestSimpleQueue {
	public static void main(String[] args) {
		SimpleQueue<Integer> q = new SimpleQueue<Integer>();
		
		System.out.println("is empty: " + q.isEmpty());
		
		for (int i = 0; i < 5; i++) q.enqueue(i);
		System.out.println("size: " + q.size());
		System.out.println("peek: " + q.peek());
		while(!q.isEmpty()) System.out.println(q.dequeue());
		for (int i = 0; i < 5; i++) q.enqueue(i);
		q.dequeue();
		q.enqueue(5);
		q.enqueue(6);
		System.out.println("size: " + q.size());
		while(!q.isEmpty()) System.out.println(q.dequeue());
		
	}
}
