////////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Server.java
// File:             SimpleListnode.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////

/**
 * Generic singly linked list node. It serves as the basic building block for 
 * storing data in singly linked chains of nodes.
 * 
 * @author Weilan Yang
 */
public class SimpleListnode<E> {
	// private fields
	private E data;
	private SimpleListnode<E> next;
	
	// constructors
	public SimpleListnode(E data) {
		this(data, null);
	}
	
	public SimpleListnode(E data, SimpleListnode<E> next) {
		this.data = data;
		this.next = next;
	}
	
	//methods
	/**
	 * Returns the current data.
	 * @return the current data
	 */
	E getData() {
		return data;
	}
	
	/**
	 * Returns the current next node.
	 * @return the current next node
	 */
	SimpleListnode<E> getNext() {
		return next;
	}
	
	/**
	 * Sets the data to the given new value.
	 * @param data the new data
	 */
	void setData(E data) {
		this.data = data;
	}

	/**
	 * Sets the next node to the given new value.
	 * @param next the new next node
	 */
	void setNext(SimpleListnode<E> next) {
		this.next = next;
	}
}
