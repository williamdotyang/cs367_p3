////////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Server.java
// File:             SimpleStack.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////

/**
 * An ordered collection of items, where items are both added
 * and removed from the top.
 * @author CS367, Weilan Yang
 */
public class SimpleStack<E> implements StackADT<E> {

    // fields
	private SimpleListnode<E> items;
	private int numItems;

	// constructors
    public SimpleStack() {
    	items = null;
    	numItems = 0;
    }

    // methods
    /**
     * Adds item to the top of the stack.
     * @param item the item to add to the stack.
     * @throws IllegalArgumentException if item is null.
     */
    public void push(E item) {
    	if (item == null) 
    		throw new IllegalArgumentException("item cannot be null!");
    	
    	if (numItems == 0) {
    		items = new SimpleListnode<E>(item);
    	} else {
    		items = new SimpleListnode<E>(item, items);
    	}
    	numItems++;
    }

    /**
     * Removes the item on the top of the Stack and returns it.
     * @return the top item in the stack.
     * @throws EmptyStackException if the stack is empty.
     */
    public E pop() {
        if (isEmpty()) throw new EmptyStackException();
        
        E item = items.getData();
        items = items.getNext();
        numItems--;
        return item;
    }

    /**
     * Returns the item on top of the Stack without removing it.
     * @return the top item in the stack.
     * @throws EmptyStackException if the stack is empty.
     */
    public E peek() {
    	if (isEmpty()) throw new EmptyStackException();
    	
    	return items.getData();
    }

    /**
     * Returns true iff the Stack is empty.
     * @return true if stack is empty; otherwise false.
     */
    public boolean isEmpty() {
    	return numItems == 0;
    }

    /**
     * Removes all items on the stack leaving an empty Stack. 
     */
    public void clear() {
    	items = null;
    	numItems = 0;
    }

    /**
     * Returns the number of items in the Stack.
     * @return the size of the stack.
     */
    public int size() {
    	return numItems;
    }
}
