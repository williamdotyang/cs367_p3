///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Server.java
// File:             Document.java
// Semester:         CS367 Fall 2015
//
// Author:           Huilin Hu, hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Weilan Yang
// Email:            wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
/////////////////////////////////////////////////////////////////////////////////

import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

/**
 * The Document class represents a single document, i.e. a 2-d numeric table, 
 * in the database. It stores all the users involved in this document and 
 * keeps track of the actual data in a two-dimensional array. 
 * 
 * @author huilin, weilan
 *
 */
public class Document {
    private String docName;
    private int rowSize;
    private int colSize;
    private int[][] doc;
    private List<User> users;

    /**
     * Constructs a document object
     * @param docName
     * @param rowSize
     * @param colSize
     * @param userList
     */
    public Document(String docName, int rowSize, int colSize, List<User>
            userList) {
    	this.docName = docName;
    	this.rowSize = rowSize;
    	this.colSize = colSize;
    	doc = new int[rowSize][colSize];
    	
    	users = new ArrayList<User>();
    	users = userList;
    	     	
    }

    /**
     * Returns the user IDs for this document.
     * @return a list of userIDs related to the document.
     */
    public List<String> getAllUserIds() {
    	List<String> userIds = new ArrayList<>();
    	
    	for (int i = 0; i < users.size(); i ++)
    		userIds.add(users.get(i).getUserId());
    	 
    	return userIds;
    	
    }
    
    /**
     * Applies the input operation to the document. 
     * @param operation
     */
    public void update(Operation operation) {
    	if (!docName.equals(operation.getDocName()))
    			throw new IllegalArgumentException("this operation doesn't apply to "
    					+ "this document");
    	
    	if (!getAllUserIds().contains(operation.getUserId()))
    		throw new IllegalArgumentException("this operation doesn't apply to "
    				+ "any user in this document");
    	
    	Operation.OP action = operation.getOp();
    	if (operation.getRowIndex() >= rowSize || operation.getRowIndex() < 0 ||
    			operation.getColIndex() >= colSize || operation.getColIndex() < 0) 
    		if (action != Operation.OP.UNDO && action != Operation.OP.REDO)
    			throw new IllegalArgumentException(
    					"the column or row index is out of bound");
    		
    	int row = operation.getRowIndex();
    	int col = operation.getColIndex();
    	int constant = operation.getConstant();
    	User user = getUserByUserId(operation.getUserId());
    	WAL current = null; 
    	
    	switch (action)	{
       		case SUB:
       			current = new WAL(row, col, doc[row][col]);
       			user.pushWALForUndo(current);
       			doc[row][col] -= constant;
        		break;
        	case ADD:
        		current = new WAL(row, col, doc[row][col]);
        		user.pushWALForUndo(current);
        		doc[row][col] += constant;
        		break;
        	case MUL:
        		current = new WAL(row, col, doc[row][col]);
        		user.pushWALForUndo(current);
        		doc[row][col] *= constant;
        		break;
        	case DIV:
        		current = new WAL(row, col, doc[row][col]);
        		user.pushWALForUndo(current);
        		doc[row][col] /= constant;
        		break;
        	case SET:
        		current = new WAL(row, col, doc[row][col]);
        		user.pushWALForUndo(current);
        		doc[row][col] = constant;
        		break;
        	case CLEAR: 
        		current = new WAL(row, col, doc[row][col]);
        		user.pushWALForUndo(current);
        		doc[row][col] = 0;
        		break;
        	case UNDO:
        		WAL undo = user.popWALForUndo();
        		int prevVal = undo.getOldValue();
        		int oldRow = undo.getRowIndex();
        		int oldCol = undo.getColIndex();
        		current = new WAL(oldRow, oldCol, doc[oldRow][oldCol]);
        		user.pushWALForRedo(current);
        		doc[oldRow][oldCol] = prevVal;
        		break;
        	case REDO:
        		WAL redo = user.popWALForRedo();
        		prevVal = redo.getOldValue();
        		oldRow = redo.getRowIndex();
        		oldCol = redo.getColIndex();
        		current = new WAL(oldRow, oldCol, doc[oldRow][oldCol]);
        		user.pushWALForUndo(current);
        		doc[oldRow][oldCol] = prevVal;
        		break;
    	}
         
    }

    /**
     * Returns the document Name.
     * @return the document Name.
     */
    public String getDocName() {
    	return docName;
    }

    /**
     * Returns the user of this document by user id. 
     * Returns null if there is no such user for this document.
     * @param userId
     * @return the user of this document by user id
     */
    public User getUserByUserId(String userId) {
    	for (User user: users) {
    		if (user.getUserId().equals(userId))
    		return user;
    	}
    	return null;	
    	
    }

    /**
     * Returns the value of the cell specified by the given row index and col index. 
     * Throws IllegalArgumentException if the index is out of bound
     * @param rowIndex
     * @param colIndex
     * @return the value of the cell specified by the given row index and col index. 
     */
    public int getCellValue(int rowIndex, int colIndex){
        if ((rowIndex >= rowSize) || (colIndex >= colSize))
        	throw new IndexOutOfBoundsException("invalid position");
        
        return doc[rowIndex][colIndex];
        	
    }

    /**
     * Returns the string representation of this document. 
     * @return the string representation of this document. 
     */
    public String toString() {
    	String docMatrix = "";
    	for (int i = 0; i < rowSize; i++)	{
    		for (int j = 0; j < colSize; j ++)	{
    			docMatrix += doc[i][j] + "\t";
    		}
    		docMatrix += "\n";
    	}
    				
        return "Document Name: " + docName + "\t" + "Size: " + "[" + rowSize + 
        		"," + colSize + "]" + "\n" + "Table: " + "\n" + docMatrix + "\n";
    }
    
}
