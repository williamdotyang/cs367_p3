////////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  WAL.java
// File:             Database.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////

/**
 * The WAL class represents the state before any operation performed. 
 * This is used to undo and redo any operation. For example, user1 changes 
 * the value of table[i][j] from a to b, then we need to create a WAL object 
 * to record which cell is changed, i.e. row index is i and col index is j, 
 * and what the previous value is, i.e. a. Later on, if the user undoes this 
 * operation, we can restore this cell based on this WAL object.
 * @author weilan
 */
public class WAL {
    // private fields
	private int rowIndex;
	private int colIndex;
	private int oldValue;

	// constructor
    public WAL(int rowIndex, int colIndex, int oldValue) {
    	this.rowIndex = rowIndex;
    	this.colIndex = colIndex;
    	this.oldValue = oldValue;
    }

    // methods
    /**
     * Returns oldValue, i.e. previous value in this cell
     * @return oldValue
     */
    public int getOldValue() {
    	return oldValue;
    }

    /**
     * Returns the row index of this cell.
     * @return rowIndex
     */
    public int getRowIndex() {
    	return rowIndex;
    }

    /**
     * Returns the column index of this cell.
     * @return colIndex
     */
    public int getColIndex() {
    	return colIndex;
    }

}
