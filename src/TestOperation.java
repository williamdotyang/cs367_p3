
public class TestOperation {

	public static void main(String[] args) {
		Operation op1 = new Operation("doc1", "user1", Operation.OP.DIV, 3, 4, 0, 12345);
		
		Operation op11 = new Operation("doc1", "user1", Operation.OP.CLEAR, 3, 4, 5, 12345);
		
		
		Operation op2 = new Operation("doc2", "user2", Operation.OP.CLEAR, 3, 4, 12345);
		Operation op22 = new Operation("doc2", "user2", Operation.OP.ADD, 3, 4, 12345);

		Operation op3 = new Operation("doc3", "user3", Operation.OP.UNDO, 12345);
		Operation op33 = new Operation("doc3", "user3", Operation.OP.CLEAR, 12345);
		
		System.out.println(op1.getDocName());
		System.out.println(op1.getUserId());
		System.out.println(op1.getOp());
		System.out.println(op1.getTimestamp());
		
		System.out.println(op1.getRowIndex());
		System.out.println(op2.getRowIndex());
		System.out.println(op3.getRowIndex());
		
		System.out.println(op1.getColIndex());
		System.out.println(op2.getColIndex());
		System.out.println(op3.getColIndex());
		
		System.out.println(op1.getConstant());
		System.out.println(op2.getConstant());
		System.out.println(op3.getConstant());
		
		
		
	}

}
