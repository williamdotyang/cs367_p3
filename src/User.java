////////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Server.java
// File:             User.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////

/**
 * A class representing users of the documents. Each user has a stack for 
 * undoing and a stack for redoing. Users are uniquely determined by userId.
 * @author weilan
 */
public class User {
    // private fields
	private String userId;
	private SimpleStack<WAL> undoStack;
	private SimpleStack<WAL> redoStack;

    public User(String userId) {
    	this.userId = userId;
    	undoStack = new SimpleStack<WAL>();
    	redoStack = new SimpleStack<WAL>();
    }

    /**
     * Returns the top WAL for undo. If there is no such WAL, returns null.
     * @return
     */
    public WAL popWALForUndo() {
    	if (undoStack.isEmpty()) return null;
    	return undoStack.pop();
    }

    /**
     * Returns the top WAL for redo. If there is no such WAL, returns null.
     * @return
     */
    public WAL popWALForRedo() {
    	if (redoStack.isEmpty()) return null;
    	return redoStack.pop();
    }

    /**
     * Pushes the WAL into undo stack. Throws IllegalArgumentException 
     * if wal is null.
     * @param trans
     * @throws IllegalArguemntException
     */
    public void pushWALForUndo(WAL trans) {
    	if (trans == null) throw new IllegalArgumentException();
    	undoStack.push(trans);
    }

    /**
     * Pushes the WAL into redo stack. Throws IllegalArgumentException 
     * if wal is null.
     * @param trans
     * @throws IllegalArgumentException
     */
    public void pushWALForRedo(WAL trans) {
    	if (trans == null) throw new IllegalArgumentException();
    	redoStack.push(trans);
    }

    /**
     * Clear redo WALs' stack.
     */
    public void clearAllRedoWAL() {
    	redoStack.clear();
    }

    /**
     * Clear undo WALs' stack.
     */
    public void clearAllUndoWAL() {
    	undoStack.clear();
    }

    /**
     * Returns the user id.
     * @return
     */
    public String getUserId() {
    	return userId;
    }
}
