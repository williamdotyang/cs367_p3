import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public class TestDocument {

	public static void main(String[] args) {
		List<User> userList = new ArrayList<>();
		userList.add(new User("user1"));
		userList.add(new User("user2"));
		userList.add(new User("user3"));
		
		Document doc1 = new Document("doc1", 5, 5, userList);
	
		System.out.println(userList.size());
		long l = 123456;
		Operation operation = new Operation("doc1", "user1", Operation.OP.ADD, 4, 2, 5,l);
		System.out.println(operation.getUserId());

		doc1.update(operation);
				
		System.out.println(doc1.toString());
	
		System.out.println(doc1.getAllUserIds());
	
		System.out.println(doc1.getDocName());
		
		System.out.println(doc1.getUserByUserId("user1"));
		System.out.println(doc1.getUserByUserId("user4"));
		
		System.out.println(doc1.getCellValue(1, 3));
		System.out.println(doc1.getCellValue(5, 2));
		
		System.out.println(doc1.toString());
		
	
		
	}

}
