///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Title:            p3
//
// Files:            Database.java, Document.java, EmptyQueueException.java, 
//					 EmptyStackException.java, Operation.java, QueueADT.java,
// 					 Server.java, SimpleListnode.java, SimpleQueue.java, 
//					 SimpleStack.java, StackADT.java, User.java, WAL.java
//
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang
// Email:            wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////////////// 80 columns wide //////////////////////////////////

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The Server class represents the high level server. It opens the input file 
 * and parses it. As it is parsing each line, it creates a Operation object 
 * and inserts the Operation into a queue. Once the entire file is parsed, 
 * the server takes Operations out of the queue one at a time and applies them 
 * to the database.This class contains the main method, you should run your 
 * program from here.
 *
 * <p>Bugs: (a list of bugs and other problems)
 *
 * @author Weilan Yang
 */
public class Server {
    // private fields
	private String inputFileName;
	private String outputFileName;
	private Database documents;
	private SimpleQueue<Operation> operationQ;
	private static final String HEADER = "----------Update Database----------";

	// constructor
    public Server(String inputFileName, String outputFileName) {
    	this.inputFileName = inputFileName;
    	this.outputFileName = outputFileName;
    	documents = new Database();
    	operationQ = new SimpleQueue<Operation>();
    }

    // methods
    /**
     * Run the server. This method will call initialize and process method.
     */
    public void run(){
        initialize();
        process();
    }

    /**
     * Initializes the server based on the information from the input file. 
     * This is where you create document objects and queue all operations in 
     * the input file.
     */
    public void initialize() {
        // parse the input file.
    	File inputFile = new File(inputFileName);
    	try {
			Scanner in = new Scanner(inputFile);
			int numOfDescription = in.nextInt();
			in.nextLine(); // skip the remaining stuff in the first line
			// parse the documents
			for (int i = 0; i < numOfDescription; i++) 
				parseDocLine(in.nextLine());
			// parse the operations
			while (in.hasNextLine()) 
				parseOpLine(in.nextLine());
			in.close();
		} catch (FileNotFoundException e) {
			System.err.println(inputFileName + " not found!");
			System.exit(-1);
		}
    }
    
    /**
     * A helper method for initialize(): parses the document description lines, 
     * and then create a Document object and add it to database.
     * @param line A string to be parsed into a document obj, comma separated
     */
    private void parseDocLine(String line) {
    	String[] tokens = line.split(",");
    	String docName = tokens[0];
    	int row = Integer.valueOf(tokens[1]);
    	int col = Integer.valueOf(tokens[2]);
    	List<User> users = new ArrayList<User>();
    	for (int i = 3; i < tokens.length; i++) users.add(new User(tokens[i]));
    	Document doc = new Document(docName, row, col, users);
    	documents.addDocument(doc);
    }
    
    /**
     * A helper method for initialize(): parses the operation description lines,
     * and then create a Operation object and add it to queue. 
     * @param line A String to be parsed into an Operation obj, comma separated 
     */
    private void parseOpLine(String line) {
    	String[] tokens = line.split(",");
    	long timestamp = Long.valueOf(tokens[0]);
    	String userId = tokens[1];
    	String docName = tokens[2];
    	Operation.OP action = Operation.OP.valueOf(tokens[3].toUpperCase());
    	
    	int row, col, constant;
    	Operation op = null;
    	switch (action) {
    	case CLEAR:
    		row = Integer.valueOf(tokens[4]);
    		col = Integer.valueOf(tokens[5]);
    		op = new Operation(docName, userId, action, row, col, timestamp);
    		break;
    	case ADD:
    	case SUB:
    	case MUL:
    	case DIV:
    	case SET:
    		row = Integer.valueOf(tokens[4]);
    		col = Integer.valueOf(tokens[5]);
    		constant = Integer.valueOf(tokens[6]);
    		op = new Operation(docName, userId, action, 
    				row, col, constant, timestamp);
    		break;
    	case UNDO:
    	case REDO:
    		op = new Operation(docName, userId, action, timestamp);
			break;
    	}
    	if (op == null) System.err.println("Operation not parsed!");
    	operationQ.enqueue(op);
    }

    /**
     * Processes each operation. Once you have queued all operations, you begin 
     * extracting one operation from the operation queue one at a time, 
     * updating the database and logging everything to the output file.
     */
    public void process() {
    	File outputFile = new File(outputFileName);
    	PrintWriter writer = null;
    	try {
			writer = new PrintWriter(outputFile);
		} catch (FileNotFoundException e) {
			System.err.println("Output file cannot be created!");
			System.exit(-1);
		}
    	// begin writing to the file
    	while (!operationQ.isEmpty()) {
			Operation op = operationQ.dequeue();
			Document doc = documents.getDocumentByDocumentName(op.getDocName());
			String operationSummary = documents.update(op);
			String content = doc.toString();
    		writer.write(HEADER + "\n");
    		writer.write(operationSummary + "\n\n");
    		writer.write(content);
    	}
		writer.close();
    }

    /**
     * The main method
     * @param args
     */
    public static void main(String[] args){
        if(args.length != 2){
            System.out.println("Usage: java Server [input.txt] [output.txt]");
            System.exit(0);
        }
        Server server = new Server(args[0], args[1]);
        server.run();
    }
}
