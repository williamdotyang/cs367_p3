////////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Server.java
// File:             SimpleQueue.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////

/**
 * An ordered collection of items, where items are added to the rear
 * and removed from the front.
 * @author Weilan Yang
 */
public class SimpleQueue<E> implements QueueADT<E> {

    // fields
	private E[] items;
	private int front;
	private int rear;
	private int numItems;
	private static final int INIT_LEN = 5;
	
	// constructors
    @SuppressWarnings("unchecked")
	public SimpleQueue() {
    	items = (E[]) new Object[INIT_LEN];
    	front = 1;
    	rear = 0;
    	numItems = 0;
    }
    
    //methods
    /**
     * Expand the array by twice the numItems as it has. 
     */
    @SuppressWarnings("unchecked")
    private void expand() {
		E[] expanded = (E[]) new Object[2 * numItems];
		System.arraycopy(items, front, expanded, 0, numItems - front);
		System.arraycopy(items, 0, expanded, numItems - front, front);
		items = expanded;
		front = 0;
		rear = numItems - 1;
    }
    
    /**
     * Correctly increment the front and rear index in various situations.
     * @param index The index to be incremented
     * @return The correct index after incrementing.
     */
    private int increment(int index) {
    	if (index == items.length - 1) return 0;
    	return index + 1;
    }

    /**
     * Adds an item to the rear of the queue.
     * @param item the item to add to the queue.
     * @throws IllegalArgumentException if item is null.
     */
    public void enqueue(E item) {
    	if (item == null)
    		throw new IllegalArgumentException("item cannot be null!");
    	if (numItems == items.length) expand();
    	rear = increment(rear);
    	items[rear] = item;
    	numItems++;
    }

    /**
     * Removes an item from the front of the Queue and returns it.
     * @return the front item in the queue.
     * @throws EmptyQueueException if the queue is empty.
     */
    public E dequeue() {
    	if (numItems == 0) throw new EmptyQueueException();
    	E item = items[front];
    	front  = increment(front);
    	numItems--;
    	return item;
    }

    /**
     * Returns the item at front of the Queue without removing it.
     * @return the front item in the queue.
     * @throws EmptyQueueException if the queue is empty.
     */
    public E peek() {
    	if (numItems == 0) throw new EmptyQueueException();
    	return items[front];
    }

    /**
     * Returns true iff the Queue is empty.
     * @return true if queue is empty; otherwise false.
     */
    public boolean isEmpty() {
    	return numItems == 0;
    }
    
    /**
     * Removes all items in the queue leaving an empty queue.
     */
    @SuppressWarnings("unchecked")
	public void clear() {
    	items = (E[]) new Object[INIT_LEN];
    	numItems = 0;
    }

    /**
     * Returns the number of items in the Queue.
     * @return the size of the queue.
     */
    public int size() {
    	return numItems;
    }
}
