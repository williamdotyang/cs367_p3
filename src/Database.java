////////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Server.java
// File:             Database.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
////////////////////////////////////////////////////////////////////////////////
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The Database class represents a single database on the server. For the 
 * purposes of this project, there is only one database object. It keeps track 
 * of all the documents on the server, and it is responsible for propagating 
 * updates to individual documents. 
 *
 * <p>Bugs: (a list of bugs and other problems)
 *
 * @author Weilan Yang
 */
public class Database {
    // private fields
	List<Document> docList;
	
	// constructors
    public Database() {
       docList = new ArrayList<Document>(); 
    }

    // methods
    /**
     * Adds one document to database. Throws IllegalArgumentException if doc 
     * is duplicated.
     * @param doc Document object to be added.
     */
    public void addDocument(Document doc) {
    	if (docList.contains(doc)) 
    		throw new IllegalArgumentException(doc + " already exists!");
    	
    	docList.add(doc);
    }

    /**
     * Returns the list of documents in the database.
     * @return
     */
    public List<Document> getDocumentList() {
    	return docList;
    }

    /**
     * Applies the given operation to the database. 
     * @param operation The Operation object to be applied.
     * @return String representation of the updated document content.
     */
    public String update(Operation operation) {
    	Document doc = getDocumentByDocumentName(operation.getDocName());
    	if (doc == null) throw new IllegalArgumentException(
    			operation + " cannot be applied to " + doc);
    	
    	doc.update(operation);
    	return operation.toString();
    }

    /**
     * Returns one document based on document id. Returns null if there is no 
     * such document in the database.
     * @param docName
     * @return Document object or null
     */
    public Document getDocumentByDocumentName(String docName) {
    	Iterator<Document> itr = docList.iterator();
    	while (itr.hasNext()) {
    		Document doc = itr.next();
    		if (doc.getDocName().equals(docName)) return doc;
    	}
    	return null; 
    }

}
