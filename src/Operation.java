///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  Server.java
// File:             Operation.java
// Semester:         CS367 Fall 2015
//
// Author:           Huilin Hu, hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Weilan Yang
// Email:            wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
/////////////////////////////////////////////////////////////////////////////////

/**
 * The Operation class represents a single change in the database.
 * A change might include creating a table, undo, addition, etc. 
 * 
 */
import java.lang.Exception;

public class Operation {
    // Enumeration of operator type.
    public enum OP {
        SET, //set,row,col,const -> set [row,col] to const
        CLEAR, //clear,row,col -> set [row,col] to 0
        ADD, //add,row,col,const -> add [row,col] by const
        SUB, //sub,row,col,const -> sub [row,col] by const
        MUL, //mul,row,col,const -> mul [row,col] by const
        DIV, //div,row,col,const -> div [row,col] by const
        UNDO, //undo the last operation
        REDO //redo the last undo
    }

    private String docName;
    private String userId;
    private OP op;
    private int rowIndex = -1;
    private int colIndex = -1;
    private int constant = -1;
    private long timestamp;

     /**
     * constructs an operation object for one of 5 operations:
     * ADD, SUB, MUL, DIV, SET
     * @param docName
     * @param userId
     * @param op
     * @param rowIndex
     * @param colIndex
     * @param constant
     * @param timestamp
     * @throws IllegalArgumentException if op is not supported, or arguments are not valid.
     * @throws ArithmeticException if op is DIV and denominator is zero.
     */
    public Operation(String docName, String userId, OP op, int rowIndex, int
            colIndex, int constant, long timestamp) {
    	   	
    	if (op == OP.CLEAR || op == OP.UNDO || op == OP.REDO)
    		throw new IllegalArgumentException("this constructor is not applicable to "
    				+ "this operation");
    	if (docName == null || userId == null || rowIndex == -1 || colIndex == -1 ||
    			constant == -1)
    		throw new IllegalArgumentException("operation cannot be applied due to "
    				+ "missing value(s)");
    	if (op == OP.DIV && constant == 0)
    		throw new ArithmeticException("the denominator cannot be zero"); 
        
        this.docName = docName;
        this.userId = userId;
        this.op = op;
        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
        this.constant = constant;
        this.timestamp = timestamp;
        	      	        
    }
    
    /**
     * constructs an operation object for CLEAR operation
     *
     * @param docName
     * @param userId
     * @param op
     * @param rowIndex
     * @param colIndex
     * @param timestamp
     * @throws IllegalArgumentException if op is not supported, or arguments are not valid.
     * 
     */
    public Operation(String docName, String userId, OP op, int rowIndex, int
            colIndex, long timestamp) {
    	if (op != OP.CLEAR)
    		throw new IllegalArgumentException("this constructor cannnot deal"
    				+ "with operations other than CLEAR");
    	if (docName == null || userId == null || rowIndex == -1 || colIndex == -1)
    		throw new IllegalArgumentException("operation cannot be applied due to "
    				+ "missing value(s)");
    	
        	this.docName = docName;
        	this.userId = userId;
        	this.op = op;
        	this.rowIndex = rowIndex;
        	this.colIndex = colIndex;
        	this.timestamp = timestamp;
        	      	
    }
    
    /**
     * constructs an operation object for UNDO or REDO operation.
     * 
     * @param docName 
     * @param userId
     * @param op
     * @param timestamp
     * @throws IllegalArgumentException if op is not supported, or arguments are not valid.
     */
    public Operation(String docName, String userId, OP op, long timestamp) {
    	if (op != OP.UNDO && op != OP.REDO)
    		throw new IllegalArgumentException("this constructor cannot deal "
    				+ "with opeartions other than UNDO and REDO");
    	if (docName == null || userId == null)
    		throw new IllegalArgumentException("operation cannot be applied due to "
    				+ "missing value(s)");
    	
        	this.docName = docName;
        	this.userId = userId;
        	this.op = op;
        	this.timestamp = timestamp;
        	      	    	
    }

    /**
     * Returns docName
     * @return docName
     */
    public String getDocName() {
        return this.docName;
     }

    /**
     * Returns user ID.
     * @return userID
     */
    public String getUserId() {
        return this.userId;
     }

    /**
     * Returns operator of this operation.
     * @return OP
     */
    public OP getOp() {
        return this.op;
    }

    /**
     * Returns the row that is involved. Returns -1 if this operation does not involve a cell.
     * @return rowIndex
     */
    public int getRowIndex() {
        return this.rowIndex;
    }

    /**
     * Returns the column that is involved. Returns -1 if this operation does not involve a cell.
     * @return colIndex
     */
    public int getColIndex() {
        return this.colIndex;
     }

    /**
     * Returns the timestamp of this operation.
     * @return timestamp
     */
    public long getTimestamp() {
        return this.timestamp;
    }

    /**
     * Returns the constant that is involved. Returns -1 if this operation does not involve a cell.
     * @return constant
     */
    public int getConstant() {
    	return this.constant;
    }

    /**
     * Returns the string representation of this operation.
     * @return the string representation of this operation.
     */
    public String toString() {
    	String opName = op.toString().toLowerCase();
        if (rowIndex == -1 && colIndex == -1)
        	return timestamp + "\t" + docName + "\t" + userId + "\t" + opName;
        else if (constant == -1)
        	return timestamp + "\t" + docName + "\t" + userId + "\t" + opName + "\t" 
        	+ "[" + rowIndex + "," + colIndex + "]";
        else
        	return timestamp + "\t" + docName + "\t" + userId + "\t" + opName + "\t" 
        	+ "[" + rowIndex + "," + colIndex + "]" + "\t" + constant; 
    }
}
