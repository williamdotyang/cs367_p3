
public class TestSimpleStack {
	public static void main(String args[]) {
		SimpleStack<Integer> s = new SimpleStack<Integer>();
		
		System.out.println("is empty: "+s.isEmpty());
		//s.pop();
		//s.peek();
		s.push(0);
		System.out.println("\nis empty: "+s.isEmpty());
		System.out.println("numItems: "+s.size());
		System.out.println("peek: " + s.peek());
		System.out.println("pop: " + s.pop());
		System.out.println("numItems: "+s.size());
		
		s.push(1);
		s.push(2);
		System.out.println("\nnumItems: "+s.size());
		System.out.println("peek: " + s.peek());
		System.out.println("pop: " + s.pop());
		System.out.println("peek: " + s.peek());
		System.out.println("numItems: "+s.size());
		
		s.clear();
		System.out.println("\nis empty: "+s.isEmpty());
		
		s.push(1);
		s.push(2);
		System.out.println("\nnumItems: "+s.size());
		
		SimpleStack<WAL> walStack = new SimpleStack<WAL>();
		WAL wal = new WAL(1, 3, 0);
		walStack.push(wal);
	}
}
